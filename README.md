 **介绍** 

cocoscreator制作的塞尔达游戏，移植于github的python版本(https://github.com/clear-code-projects/Zelda)

 **游戏引擎** 

cocoscreator 2.4.7

 **按键**

1：移动 箭头上下左右 

2：武器攻击 space / A 

3：魔法攻击 ctrl / B 

4：切换武器 q / X 

5：切换魔法 e / Y 

6：角色升级界面 m，再按m取消 

 **游戏截图：** 

![输入图片说明](screenshots/0.png)
![输入图片说明](screenshots/1.png)
![输入图片说明](screenshots/2.png)
![输入图片说明](screenshots/3.png)