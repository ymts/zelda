let GameEvent = {
    loadResEnd : "loadResEnd",
    key_menu : "key_menu",
    game_pad_down : "game_pad_down",
    game_pad_up : "game_pad_up",
}

export default GameEvent