import GameEvent from './GameEvent';
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

let COOL_DOWN = 0.3

@ccclass
export default class GamePad extends cc.Component {


    @property(cc.Node)
    dirNode : cc.Node = null

    @property(cc.Node)
    keyNode : cc.Node = null

    touch_key_name = ""
    dir_cool_down = 0

    protected onLoad(): void {
        this.dirNode.active = false    
        this.keyNode.active = false


        let dirKeyList = ["up", "down", "left", "right"]
        for(let i=0; i<dirKeyList.length; i++) {
            this.registerDirBtn(dirKeyList[i])
        }
    }

    registerDirBtn(key) {


        let keyList = {
            "up":cc.macro.KEY.up,
            "down":cc.macro.KEY.down,
            "left":cc.macro.KEY.left,
            "right":cc.macro.KEY.right,
        }
        let changeKey = keyList[key]

        let btn = this.dirNode.getChildByName("btn_" + key)
        btn.on(cc.Node.EventType.TOUCH_START, ()=>{
            this.touch_key_name = key
            this.dir_cool_down = 0
            this.onBtnClick(null, this.touch_key_name)
        }, this)

        btn.on(cc.Node.EventType.TOUCH_END, ()=>{
            this.touch_key_name = ""
            cc.director.emit(GameEvent.game_pad_up, changeKey)
        }, this)

        btn.on(cc.Node.EventType.TOUCH_CANCEL, ()=>{
            this.touch_key_name = ""
            cc.director.emit(GameEvent.game_pad_up, changeKey)
        }, this)
        
    }


    update(dt) {
        this.dir_cool_down += dt

        if(dt > COOL_DOWN) {
            if(this.touch_key_name != "") {
                this.onBtnClick(null, this.touch_key_name)
                this.dir_cool_down = 0
            }
        }

    }

    onBtnClick(target, isFrom) {

        switch(isFrom) {
            case "up":
                cc.director.emit(GameEvent.game_pad_down, cc.macro.KEY.up)
                break

            case "down":
                cc.director.emit(GameEvent.game_pad_down, cc.macro.KEY.down)
                break

            case "left":
                cc.director.emit(GameEvent.game_pad_down, cc.macro.KEY.left)
                break

            case "right":
                cc.director.emit(GameEvent.game_pad_down, cc.macro.KEY.right)
                break

            case "a":
                cc.director.emit(GameEvent.game_pad_down, cc.macro.KEY.space)
                break

            case "b":
                cc.director.emit(GameEvent.game_pad_down, cc.macro.KEY.ctrl)
                break
                        
            case "x":
                cc.director.emit(GameEvent.game_pad_down, cc.macro.KEY.q)
                break
                            
            case "y":
                cc.director.emit(GameEvent.game_pad_down, cc.macro.KEY.e)
                break                            

            case "m":
                cc.director.emit(GameEvent.key_menu)
                break     
        }

    }

    onBtnShow() {
        this.dirNode.active = !this.dirNode.active    
        this.keyNode.active = !this.keyNode.active    
    }

}
