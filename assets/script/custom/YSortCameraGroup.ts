// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Group from "./Group";
import Player from '../Player';
import Sprite from './Sprite';

const {ccclass, property} = cc._decorator;

@ccclass
export default class YSortCameraGroup extends Group {

    offset = cc.v2(0, 0)
    half_width = 3648 / 2
    half_height = 3200 / 2
    floor_rect
    floor_offset_pos

    @property(cc.Sprite)
    bg : cc.Sprite = null

    addSprite(child : Sprite) {
        this.node.addChild(child)
        this.childList.push(child)
    }

    
	custom_draw(player : Player) {
		// self.offset.x = player.rect.centerx - self.half_width
		// self.offset.y = player.rect.centery - self.half_height

		// # drawing the floor
		// floor_offset_pos = self.floor_rect.topleft - self.offset
		// self.display_surface.blit(self.floor_surf,floor_offset_pos)

		// # for sprite in self.sprites():
		// for sprite in sorted(self.sprites(),key = lambda sprite: sprite.rect.centery):
		// 	offset_pos = sprite.rect.topleft - self.offset
		// 	self.display_surface.blit(sprite.image,offset_pos)

        let playerX = player.x
        let playerY = player.y

        this.offset.x = playerX - this.half_width
        this.offset.y = playerY - this.half_height

        // this.floor_offset_pos = cc.Vec2(this.flo)
        let levelNode = cc.find("Canvas/map/level")
        levelNode.setPosition(cc.v2(-playerX, -playerY))
        player.image.setPosition(player.rect.center)

        // let rect = cc.find("Canvas/map/level/visible_sprites/bg").getBoundingBox()
        // console.log(rect)

        // 上下进行排序
        for(let sprite of this.childList) {
            sprite.zIndex = -sprite.y + 10000
        }

        return
    }


    enemy_update(player : Player) {

        let enemy_sprites = []
        for(let child of this.childList) {
            // @ts-ignore
            if(child.sprite_type != null && child.sprite_type == "enemy" ) {
                enemy_sprites.push(child)
            }
        }

        for(let enemy of enemy_sprites) {
            enemy.enemy_update(player)
        }

    }

}
