import AnimationPlayer from './particles';
import Global from './settings';
import { randomInt, randomBetween } from './support';
import Player from './Player';
import SoundManager from './custom/SoundManger';
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html


let TILESIZE = Global.TILESIZE

export default class MagicPlayer  {


    sounds = {
		'heal': 'heal',
		'flame':'fire'
    }

    animation_player : AnimationPlayer

    constructor(animation_player : AnimationPlayer) {
		this.animation_player = animation_player
    }

    heal(player,strength,cost,groups) {
        if(player.energy >= cost) {
            SoundManager.getInstance().playEffect(this.sounds['heal'])
            player.health += strength
            player.energy -= cost
            if(player.health >= player.stats['health']) {
                player.health = player.stats['health']
            }
            this.animation_player.create_particles('aura',[player.rect.center.x, player.rect.center.y],groups)
            this.animation_player.create_particles('heal',[player.rect.center.x, player.rect.center.y],groups)
        }
    }

    flame(player : Player,cost,groups) {
        if(player.energy >= cost) {
            player.energy -= cost
            SoundManager.getInstance().playEffect(this.sounds['flame'])

            let direction = cc.v2(0, 0)

            if(player.status.split('_')[0] == 'right'){ 
                direction = cc.v2(1,0) 
            } else if(player.status.split('_')[0] == 'left') { 
                direction = cc.v2(-1,0) 
            } else if(player.status.split('_')[0] == 'up') { 
                direction = cc.v2(0,1)
            } else { 
                direction = cc.v2(0,-1)
            }

            for(let i=0; i<6; i++) {
                if(direction.x)  { // horizonta)
                    let offset_x = (direction.x * (i+1)) * TILESIZE
                    let x = player.rect.center.x + offset_x + randomBetween(-TILESIZE / 3, TILESIZE / 3)
                    let y = player.rect.center.y + randomBetween(-TILESIZE / 3, TILESIZE / 3)
                    this.animation_player.create_particles('flame',[x,y],groups)
                }else { // vertical
                    let offset_y = (direction.y * (i+1)) * TILESIZE
                    let x = player.rect.center.x + randomBetween(-TILESIZE / 3, TILESIZE / 3)
                    let y = player.rect.center.y + offset_y + randomBetween(-TILESIZE / 3, TILESIZE / 3)
                    this.animation_player.create_particles('flame',[x,y],groups)
                }
            }
        }
    }
}
