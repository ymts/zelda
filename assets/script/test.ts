// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Test extends cc.Component {

    @property(cc.Sprite)
    testNode: cc.Sprite = null;

    onLoad() {
        // this.testNode.spriteFrame.setFlipX(true)

        this.testNode.spriteFrame.getTexture().packable = false
        this.testNode.spriteFrame.getTexture().setFlipY(true)

        console.log(this.testNode.spriteFrame.getTexture().packable)

    }

    // update (dt) {}
}
