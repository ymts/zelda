import Sprite from './custom/Sprite';
import { ResManager } from './custom/ResManager';
import Player from './Player';
import { addToGroups } from './support';
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html


export default class Weapon extends Sprite {

    sprite_type
    image : cc.Node

    constructor(player : Player, groups) {

        super(groups)

        this.sprite_type = 'weapon'
        let direction = player.status.split('_')[0]

        let res = ResManager.getInstance().getRes()
        let directRes = res[player.weapon]
        let node = this
        let sp = node.addComponent(cc.Sprite)
        sp.spriteFrame = directRes[direction]
        // @ts-ignore
        node.script = this
        this.image = this

        
        this.rect = this.image.getBoundingBox()
        let width = this.rect.width
        let height = this.rect.height

        // placement
        if(direction == 'right') {
            this.image.setPosition(player.rect.xMax+width/2, player.rect.yMin+16)
        }
        else if(direction == 'left') {
            this.image.setPosition(player.rect.xMin-width/2, player.rect.yMin+16)
        }
        else if(direction == 'down') {
            this.image.setPosition(player.rect.xMin+22, player.rect.yMin-height/2+4)
        }
        else{
            this.image.setPosition(player.rect.xMin+22, player.rect.yMax+height/2)
        }
        
        addToGroups(this.image, groups)

    }

}
