// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameEvent from "./custom/GameEvent";
import { ResManager } from "./custom/ResManager";
import Level from "./level";


const {ccclass, property} = cc._decorator;

@ccclass
export default class main extends cc.Component {

    level : Level = null;

    isHasLoadRes = false
    
    resManager : ResManager = null

    gamePad : cc.Node = null

    @property(cc.Node)
    bg : cc.Node = null


    protected onLoad(): void {
        this.init()
    }

    init() {

        cc.director.on(GameEvent.loadResEnd, this.loadRedEnd, this)

        this.level = cc.find('map/level', this.node).getComponent(Level)
        this.gamePad = cc.find('map/UI_Gamepad', this.node)

        ResManager.getInstance().load()

        this.bg.active = false
        this.gamePad.active = false

        cc.game.setFrameRate(60)

    }

    start () {
        
        cc.resources.load("audio/main", cc.AudioClip, (err, asset:cc.AudioClip)=>{
            cc.audioEngine.playMusic(asset, true)
            cc.audioEngine.setMusicVolume(0.5)
            cc.audioEngine.setEffectsVolume(0.5)
        })
    }

    loadRedEnd() {
        this.level.init()
        this.isHasLoadRes = true
        this.bg.active = true
        this.gamePad.active = true
    }

    protected update(dt: number): void {

        if(!this.isHasLoadRes) {
            return
        }

        this.level.run(dt)
    }

}
